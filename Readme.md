# Modul 231
### Inhaltverzeichnis 
## Pinguine
*[Auftrag S1](Aufgaben/Pinguine.md)  
## CIA
* [Auftrag S7](Aufgaben/CIA.md) 
  
## Checkliste
* [Auftrag S5](Aufgaben/Checkliste.md)
  
## Das Internet als Datenarchiv
* [Auftrag S6](Aufgaben/Das%20Internet%20als%20Datenarchiv.md)
  
## Authentifizierung
* [ Auftrag S8](Aufgaben/Authentifizierung.md)
 
## Passwort hacken
* [Auftrag S9](Aufgaben/Passworthacken.md)
  
## Backup
* [Auftrag S11](Aufgaben/Backup.md)
  
## Passwortmanager
* [Auftrag S10](Aufgaben/Passwortmanager.md)
  
  ## Verschlüsselung
* [Auftrag S12](Aufgaben/Verschlüsselung.md)
  
  ## Foxtrail
* [-Foxtrail](Aufgaben/Foxtrails.md)
  ## Urheberrecht
* [-Urheberrecht](Aufgaben/Urheber.md)
  ## Veracrypt
* [Auftrag S14](Aufgaben/Veracrypt.md)
  
  

### Kommentar von Silvan Steinauer
- Das Inhaltsverzeichniss, gibt Auskunft und dient sehr schön als Shortcut zu den einzelnen Aufträgen. Der Umfang vom Respository ist sehr gut, fast alle Aufträge sind gemacht. In den Aufträgen hast du auch meistens eine Reflexion gemacht, was sehr gut ist. Verbessern hättest du manchmal die Struktur in den Aufträgen. Trotzdem ist es sehr gelungen.