### Pinguine
![Pinguine](/Bilder/Pinguine.jpg)

Die Pinguine (Spheniscidae) sind eine Gruppe flugunfähiger [Seevögel](https://de.wikipedia.org/wiki/Meeresvogel) der [Südhalbkugel](https://de.wikipedia.org/wiki/S%C3%BCdhalbkugel) und bilden die einzige Familie in der Ordnung Sphenisciformes. Ihre stammesgeschichtliche Schwestergruppe bilden wahrscheinlich die Seetaucher (Gaviiformes) und Röhrennasen (Procellariiformes). Pinguine sind leicht von allen anderen Vögeln zu unterscheiden und in herausragender Weise an das Leben im Meer und in den teilweise extremen Kältezonen der Erde angepasst.

## Spiele



Folgende Browserspiele beinhalten **Pinguine** als Entities:

![](Bilder/Tabelle.png)

 <br>  



## Pinguin-Gattungen



* Langschwanzpinguine



  * Eselpinguine



  * Adeliepinguine



* Grossschnabelpinguine



  * Königspinguine



  * Kaiserpinguine



* Schopfpinguine



  * Dickschnabelpinguine



  * Snarespinguine



  * Kronenpinguine



  * Haubenpinguine



  * Goldschopfpinguine



  * Felsenpinguin



  * Tristanpinguin



## Aufzucht



Die Aufzucht der jungen Pinguine lässt sich in zwei Phasen unterteilen:



1. In den ersten zwei bis Wochen werden die Kücken permanent von einem Elternteil beaufsichtigt.



2. Sobald die Jungtiere herangewachseln sind, beginnt die "Kindergarten"-Zeit. Die Jungen schliessen sich in Gruppen zusammen, während die Alttiere versuchen Nahrung herbeizuschaffen.



## Welt-Pinguin-Tag



Sprüche zum Welt-Pinguin-Tag





Das Schlimme dran ein Pinguin zu sein ist, dass wenn man sauer ist und wegwatschelt, immer noch total süss aussieht.





Pinguin: "Ich hätte gerne Passbilder." Fotograf: "Schweiz-weiss oder in Farbe?"





Drei von vier Stimmen in meinem Kopf wollen schlafen. Eine möchte unbedigt wissen, ob Pinguine Knie haben.





## Hallo-Pinguin-Programm



Hier findet ihr den Code für das Hallo Pinguin Programm in Python:



```Python



import pyfiglet



ascii_banner = pyfiglet_format("Hallo Pingu!")



print(ascii_banner)



```





##Offene Todos



* [ ] ~~Install Script~~ Add requirements.txt



* [ ] Use *graffiti* font



* [ ] Add English translation (Hello Pingu)

 ## Reflexion
 Ich habe alle Funktionen, die man für das Repository benötigt, gut verstanden