## Backup
### Was ist ein Backup?
Ein Backup dient zur Datensicherung, so dass man die Daten bei einem Verlust wieder herstellen kann.
### Wieso sollte man ein Backup erstellen?
-Man muss Arbeiten nicht zweimal machen, falls die Daten verloren gehen.
-Es ist einfach die Daten wiederherzustellen.
### Worauf kann man ein Backup speichern. Was sind die Vor/Nachteile?

|Cloud|Externe Festplatte|
|----------|---------|
|Vorteile:|Vorteile|
|-Die Daten lassen sich einfach verwalten.|-Man weiss immer wo sich die Daten befinden.
|-Man hat von allen Orten Zugriff auf die Daten.|Nachteile:
|-Viel Speicherplatz|-Wenn die Festplatte geklaut oder zerstört wird sind die Daten weg.
|Nachteile:|-Hohe kosten.
|-Man weiss nicht was mit den Daten passiert.|
![](../Bilder/Backup.png)
## Backup Verfahren
### Inkrementelles Backup
Nach dem Vollbackup werden nur die Änderungen gespeichert. Das heisst es werden nur die veränderten oder hinzugefügten Daten gespeichert.
### Differntielles Backup
Zuerst wird ein Backup von allen Daten erstellt, danach werden bei jeder Änderung die neuen Daten hinzugefügt. Die bereits vorhandenen Daten werden nicht nocheinmal gespeichert.
### Vollbackup
Ein Vollbackup ist eine Momentaufnahme aller ausgewählten Daten und bildet die Grundlage für ein inkrementelles und differentielles Backup.

## Reflexion
Ich habe diese Aufgabe gut verstanden, nur bei dem Unterschied von einem inkrementellen und einem differentiellen Backup hatte ich zuerst ein wenig Mühe.