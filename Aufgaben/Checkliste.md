# Daten im Homeoffice schützen

## Mit diesen 8 Massnahmen schützt man seine Daten

### 1. Installieren sie alle Updates

Ja, ich habe alle Updates über die Einstellungen installiert

### 2. Verwenden sie starke Passwörter

Ja, ich habe ein starkes Passwort. Es sollte gross und kleinschreibung enthalten zusätzlich noch Zahlen und Sonderzeichen.

### 3. Geben Sie Passwörter nicht weiter

Niemand weiss mein Passwort. Es ist eigentlich klar das man es niemandem geben sollte.

### 4. Schützen Sie Personendaten und geschäftliche Informationen

Ich habe meine persönlichen Daten nicht gesichert, aber sperre immer den Bildschirm

Man sollte das gerät nie offen stehen lassen, also sollte man immer den Bildschirm sperren.

Zusätzlich sollte man seine Daten auf einem verschlüsselten USB-Stick oder einer externen Festplatte speichern. Auch die Daten auf dem Gerät sollten immer verschlüsselt sein.

### 5. Setzten Sie E-Mail sicher ein

Ich habe eine Private und eine geschäftliche E-Mail. Sie sind von zwei verschiedenen Anbietern.

### 6. 

Für geschäftliche Themen sollte man Tools verwenden in denen man sicher Dateien teilen kann und gleichzeitig Unterhaltungen führen kann. Dazu verwende ich Microsoft Teams. 

Privat schreibe ich über Discord oder whatsapp.

### 7.

Ich öffne keine Links bei denen ich nicht weiss um was es sich handelt, oder nicht weiss von wem die datei kommt.

### 8.

Wenn plötzlich Daten fehlen melde ich das sofort.

# smartphone-Sicherheit erhöhen

## Mit diesen 7 Schritten erhöht man die sicherheit auf ihrem Smartphone

### 1. Gerätesperre aktivieren

Man sollte die Gerätesperre aktivieren, so dass man es nur mit dem Passwort oder PIN entsperrren kann.

Ich habe ihn auf 2 Minuten eingestellt.

### 2. Gerät bei Verlust sofort sperren und löschen lassen

Man sollte die Funktion haben das man das Gerät aus der Ferne zurücksetzten kann. Zusätzlich sollte man seine SIM-Karte sperren lassen

### 3. Apps Apps aus vertrauenswürdigen Quellen installieren

Apps sollte man z.B. nur aus dem Play- oder Appstore herunterladen.

### 4. Öffentliche Funknetzwerke mit Vorsicht nutzen

In öffentlichen Hotspots sollte man keine kritzischen Anwendungen wie z.B. Online-banking durchführen.

### 5. Regelmässige Updates

Die Sicherheitsupdates sollten immer auf dem neusten Stand seni.

### 6. Daten vor Verkauf oder Entsorgung komplett löschen

Man sollte das Gerät vor dem Verkauf komplett zurücksetzen, damit alle Daten gelöscht werden.

### 7. Drahtlose Schnittstellen deaktivieren

Wenn sie die Drahtlosen Schnittstellen, wie z.B. WLAN oder Bluetooth nicht benutzen, dann deaktivieren sie diese.



