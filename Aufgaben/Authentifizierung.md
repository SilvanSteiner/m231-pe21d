## Authentifizierung

Bei der authentifizierung wird überprüft welche Person man ist. Dazu dienen Benutzernamen und Passwörter.

## Autorisieren

Bei der Autorisierung wird überprüft ob die Person die nötigen Rechte hat um den Zugriff zu bekommen. Diese wird zur nachgewiesenen Identität zugewiesen.

## Reflexion
Es geht in dieser Aufgabe nur um das Verständnis. Es war nicht so schwer um es zu verstehen.