# Lizenzen

## Was ist eine Lizenz
Eine Lizenz ist eine Art von Vertrag oder Erlaubnis, die es einer Person oder Organisation ermöglicht, etwas Bestimmtes zu nutzen. Dies kann beispielsweise die Nutzung von Software, Musik, Filmen oder Bildern umfassen. Lizenzen können unter bestimmten Bedingungen erworben werden und beschränken in der Regel die Art und Weise, wie das lizenzierte Gut genutzt werden darf. Es gibt verschiedene Arten von Lizenzen, wie z.B. Open-Source-Lizenzen oder kommerzielle Lizenzen.

## Verschiedene Lizenzen
### Freeware
Freeware ist Software, die kostenlos heruntergeladen und verwendet werden kann, ohne dass dafür eine Gebühr an den Entwickler oder Herausgeber gezahlt werden muss. Es gibt jedoch häufig Einschränkungen bei der Nutzung der Software, wie z.B. die Unmöglichkeit, den Quellcode zu ändern oder die Software zu verkaufen. Freeware wird oft von Einzelpersonen oder kleinen Unternehmen entwickelt und kann in der Regel ohne Support oder Garantie verwendet werden.
### Public Domain
Die Public Domain-Lizenz, auch als Public Domain Mark oder Public Domain Dedication genannt, ermöglicht es jedermann, ein bestimmtes Werk, wie z.B. ein Buch, ein Musikstück oder ein Film, zu verwenden, ohne dass dafür eine Erlaubnis des Urhebers oder eine Gebühr anfallen. Ein Werk unterliegt der Public Domain, wenn es keinen gültigen Schutzrechte mehr besitzt, d.h. wenn es kein Copyright oder Schutzrechte mehr besitzt. Ein Werk, das in die Public Domain eintritt, kann von jedermann verwendet, kopiert und verbreitet werden, ohne dass dafür eine Erlaubnis eingeholt werden muss. 
### Open Source Lizenz
Open-Source-Lizenz ist eine Art von Softwarelizenz, die es ermöglicht, den Quellcode einer Software einzusehen, zu bearbeiten und zu verteilen. Open-Source-Software darf in der Regel von jedermann verwendet, kopiert und weiterverteilt werden, solange bestimmte Bedingungen erfüllt werden. Diese Bedingungen sind je nach Lizenz unterschiedlich, können aber beispielsweise die Pflicht umfassen, die Quellcodeänderungen ebenfalls zur Verfügung zu stellen, oder die Einschränkung, die Software nicht für kommerzielle Zwecke zu verwenden.
### Freie Softwarelizenz
Freie Software-Lizenz ist eine Art von Open-Source-Lizenz, die es ermöglicht, die Software frei zu verwenden, zu bearbeiten und zu verteilen. Im Unterschied zu Freeware, wo man die software kostenlos herunterladen kann aber nicht immer die Möglichkeit hat, den Quellcode zu sehen, bearbeiten und weitergeben, ermöglicht die Freie Software-Lizenz die vollständige Freiheit der Nutzung, Veränderung und Verbreitung der Software.
### Donationware
Donationware ist eine Art von Software, bei der die Verwendung der Software kostenlos ist, aber der Entwickler oder Herausgeber die Möglichkeit bietet, eine Spende oder eine finanzielle Unterstützung zu leisten, um die Entwicklung und Unterstützung der Software fortzusetzen.
### Kommerzielle Software
Kommerzielle Software ist eine Art von Software, die für den Kauf oder die Nutzung gegen Bezahlung angeboten wird.
### Proprietäre Lizenz
Eine proprietäre Lizenz ist eine Art von Softwarelizenz, bei der die Rechte zur Verwendung, Veränderung und Verbreitung der Software eingeschränkt sind und der Quellcode nicht verfügbar ist. Nutzer müssen in der Regel eine Lizenz erwerben, um die Software zu verwenden und sich an die Lizenzbedingungen halten. 
### Standardlizenz
Eine Standardlizenz ist eine allgemein verwendete Art von Lizenz, die die Nutzungsbedingungen für ein bestimmtes Produkt, in der Regel Software, festlegt. Diese Art von Lizenz gibt dem Nutzer das Recht, das Produkt zu verwenden, aber es kann bestimmte Einschränkungen geben, wie die Anzahl der erlaubten Installationen oder die Dauer des Lizenzvertrags. Eine Standardlizenz kann auch beschränkungen beinhalten, wie das Produkt verwendet werden darf, z.B. darf es nicht für kommerzielle Zwecke verwendet werden oder es ist nicht erlaubt es zu modifizieren.