## Urheberrecht
* Recht zu entscheiden, was mit dem Lichtbild passiert
* Recht zur Namensnennung
### Urgheber
Ein Urheber ist jemand der etwas neues geschaffen hat
## Nutzungsrecht
* Recht zur Veröffentlichung
* Recht zur Bearbeitung
* Verwertungsrecht
## Was ist geschützt?
Geschützt ist die Art und Weise, in welcher eine Idee zum Ausdruck gelangt, nicht aber die Idee oder das Konzept selbst.
* Bilder 
* Filme
* Musik
* Quellcode
* Baukunst
* Pläne
* Usw.
* 70 Jahre lang gültig
* 50 Jahre bei Computerprogrammen
## Nicht geschützt
* Ideen
* Anweisungen
* Entstehung
## Entstehung des Urheberrechts
* Keine Anmeldung
* Kein Register
## Ausnahmen
* Zitatrecht
* Dekompilierung (Zum beschaffen von Informationen über Schnittsellen)
* Archivierung und Sicherheitskopien
### Impressum
* Name, Vorname oder Firmenname
* Postadresse
* E-mail Adresse
* Optional: Telefonnummer
#### Wer muss ein Impressum haben?
Jeder der eine Webseite zu geschäftlichen Zwecken nutzt.
#### Beispiel Swisscom
![](Bilder/../../Bilder/Swisscom.png)