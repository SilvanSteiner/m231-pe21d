## Verschlüsselung
### Kleopatra
Kleopatra wird dazu verwendet Schlüssel zu erstellen, importieren und exportieren. Man kann durch Kleopatra Dateien verschlüsseln und entschlüsseln, solange man den richtigen Schlüssel besitzt.
### Vorgehen
#### 1. Beide müssen einen Schlüssel erstellen, in dem sie unter "Dateien", "Neues PGP-Schlüsselpaar erstellen" wählt und ihren Namen und eine E-mail angibt.
   ![1](../Bilder/1.png)
   ![2](../Bilder/2.png)

#### 2. Diesen sendet man sich und importiert ihn anschliessend. 
   ![3](../Bilder/3.png)

#### 3. Jetzt kann man eine Datei verschlüsseln und muss dabei die zuvor importierte Person beglaubigen.
   ![4](../Bilder/4.png)

#### 4. Jetzt sendet man die Datei an die andere Person. 
   ![5](../Bilder/5.png)

#### 5. Diese Person muss die Datei entschlüsseln. Das geht wenn man den Schlüssel des Dateierstellers importiert hat.
   ![6](../Bilder/6.png)

Jetzt sollte die Datei unentschlüsselt sein und man kann sie öffnen.
### Reflexion
Mit dieser Aufgabe hatte ich zuerst ein wenig Schwierigkeiten, doch nachdem mir Manuel alles noch einmal erklärt hat, hatte ich keine Probleme mehr.