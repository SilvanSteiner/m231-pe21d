# CIA
## C
### -Vertraulichkeit
Die Vertraulichkeit besteht darin, dass niemand unbefugtes Zugriff auf diese Daten hat.  
Probleme:  
Die Daten wurden veröffentlicht.  

## I
### -Integrität
Die Integrität besteht aus der Korrektheit der Daten.  
Probleme:  
Die Daten wurden verfälscht.
## A
### -Verfügbarkeit
Die Verfügbarkeit besteht darin, dass man immer auf seine Daten Zugriff hat.   
Probleme:  
Wenn die Daten verloren gehen und man kein Backup hat.  
Wenn die Daten durch eine Ransomware blockiert wird.  
Wenn die Daten auf einem Server nicht erreichbar sind (z.B. bei einem Stromausfall).

# Reflexion

Diese Aufgabe war nur eine Frage des Verständnis. Das CIA Prinzip habe ich gut verstanden.